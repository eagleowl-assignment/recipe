# EagleOwl Assignment

## [Live Site](https://recipes-eagleowl.netlify.app)

## Tech stack
- React JS, SCSS, React-Router, React-LazyLoad

## Setup
- Install all project dependencies with ```npm install```
- Start the development server with ```npm start```
- Transpile SCSS to CSS with transpiler. (I've used Live Sass Compiler Extension present in VS Code Editor)
- Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Note: This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Tasks Completed
- [x] Single Page App(SPA)
- [x] Integrate lazy-loading in the list component.
- [x] The checkbox on the column of the list component should select all the loaded rows.
- [x] Some hovering interactions should be added to the table rows.
- [x] Integrate API's

## API & End Points used :
- page
- is_incorrect
- is_untagged
- id_disabled
- order

All the given API's used.