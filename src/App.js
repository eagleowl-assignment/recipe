import React from 'react';
import { Switch, Route } from "react-router-dom";
import Cards from "./Components/Cards/Cards";
import Navbar from "./Components/Navbar";
import AllRecipes from "./Components/Tables/AllRecipes";
import Incorrect from "./Components/Tables/Incorrect";
import Untagged from "./Components/Tables/Untagged";
import Disabled from "./Components/Tables/Disabled";

const App = () => {

  return (
    <>
      <Cards />
      <Navbar />
      <Switch>
        <Route exact path="/" component={AllRecipes} />
        <Route exact path="/incorrect" component={Incorrect} />
        <Route exact path="/untagged" component={Untagged} />
        <Route exact path="/disabled" component={Disabled} />
      </Switch>
    </>
  )
}

export default App
