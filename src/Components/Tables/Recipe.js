import React from 'react'

export default function Recipe(props) {

  return (
    <>
      <div className="recipe">
        <div className="recipe-checkbox"><input type="checkbox" name={props.name} checked={props.isChecked.includes(props.name)}
          onChange={props.handleSingleCheck} /></div>
        <div className="recipe-name">{props.name}</div>
        <div className="recipe-updt">{props.lastUpdated}</div>
        <div className="recipe-cogs">{props.cogs}%</div>
        <div className="recipe-cp">&#8377; {props.cp.toFixed(2)}</div>
        <div className="recipe-sp">&#8377; {props.sp.toFixed(2)}</div>
        <div className="recipe-gross">{props.gross.toFixed(0)}%</div>
        <div className="recipe-tags"><span>Continental</span></div>
      </div>
    </>
  );
}
