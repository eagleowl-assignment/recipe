import Recipe from "./Recipe";
import TableHead from "./TableHead";
import useFetch from "../CustomHooks/useFetch";
import useCheckbox from "../CustomHooks/useCheckbox";
import LazyLoad from 'react-lazyload';

const Loading = () => {
  return (
    <div>Loading...</div>
  )
}

const Incorrect = () => {

  const url = 'https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/?page=2&is_incorrect=false';
  const { recipe } = useFetch(url);
  const { handleSingleCheck, handleAllCheck, allChecked, isChecked } = useCheckbox(recipe);

  return (
    <>
      <div className="box">
        <div className="table-main">
          <TableHead checked={allChecked} onChange={handleAllCheck} />

          {recipe.map((dish, index) => {
            const date = new Date(dish.last_updated.date);
            return (
              <LazyLoad key={index} resize={true} placeholder={<Loading />}>
                <Recipe
                  key={index}
                  name={dish.name}
                  lastUpdated={date.toDateString()}
                  cogs={dish.cogs}
                  cp={dish.cost_price}
                  sp={dish.sale_price}
                  gross={dish.gross_margin}
                  handleSingleCheck={handleSingleCheck}
                  isChecked={isChecked}
                />
              </LazyLoad>
            );
          })
          }

        </div>
      </div>
    </>
  );
}

export default Incorrect;

