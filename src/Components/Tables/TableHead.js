import React from 'react'

const TableHead = (props) => {

    return (
        <div className="recipe head">
            <div className="recipe-checkbox">
                <input name="checkall" type="checkbox" checked={props.checked} onChange={props.onChange} /></div>
            <div className="recipe-name">NAME</div>
            <div className="recipe-updt">LAST UPDATED</div>
            <div className="recipe-cogs">COGS%</div>
            <div className="recipe-cp">COST PRICE</div>
            <div className="recipe-sp">SALE PRICE</div>
            <div className="recipe-gross">GROSS MARGIN</div>
            <div className="recipe-tags">TAGS/ACTIONS</div>
        </div>
    )
}

export default TableHead;
