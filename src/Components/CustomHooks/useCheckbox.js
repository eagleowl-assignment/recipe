import { useState } from 'react'

const useCheckbox = (recipe) => {

    const [allChecked, setAllChecked] = useState(false);
    const [isChecked, setIsChecked] = useState([]);

    const handleAllCheck = e => {
        if (allChecked) {
            setAllChecked(false);
            return setIsChecked([]);
        }

        setAllChecked(true);
        return setIsChecked(recipe.map(data => data.name));
    };

    const handleSingleCheck = e => {
        const { name } = e.target;
        if (isChecked.includes(name)) {
            setIsChecked(isChecked.filter(checked_name => checked_name !== name));
            return setAllChecked(false);
        }

        isChecked.push(name);
        setIsChecked([...isChecked]);
        setAllChecked(isChecked.length === recipe.length)
    };

    return {
        handleSingleCheck,
        handleAllCheck,
        allChecked,
        isChecked
    };
}

export default useCheckbox;
