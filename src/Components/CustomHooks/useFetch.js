import { useState, useEffect } from 'react';

const useFetch = (url) => {

    const [recipe, setRecipe] = useState([]);

    useEffect(() => {

        const abortController = new AbortController();

        fetch(url, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },

            signal: abortController.signal
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (myJson) {
                setRecipe(myJson.results);
            })
            .catch(err => {
                if (err.name === 'AbortError') {
                    console.log("Api call aborted");
                }
                else {
                    console.log(err);
                };
            });

        return () => abortController.abort();
    }, [setRecipe, url]);

    return {
        recipe
    }
}

export default useFetch;
