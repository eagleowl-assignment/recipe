import React from 'react';
import { NavLink } from "react-router-dom";

const Navbar = () => {
    return (
        <nav>
            <NavLink exact to="/" activeClassName="active-link">All Recipes</NavLink>
            <NavLink exact to="/incorrect" activeClassName="active-link">Incorrect</NavLink>
            <NavLink exact to="/untagged" activeClassName="active-link">Untagged</NavLink>
            <NavLink exact to="/disabled" activeClassName="active-link">Disabled</NavLink>
        </nav>
    )
}

export default Navbar;
