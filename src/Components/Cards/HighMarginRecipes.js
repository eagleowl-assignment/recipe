import React from 'react';
import useFetch from "../CustomHooks/useFetch";

const HighMarginRecipes = () => {

    const url = 'https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/margin-group/?order=top';
    let { recipe } = useFetch(url);

    return (
        <>
            <div className="container">
                <h3>High Margin Recipes</h3>
                <div className="circle-container">
                    {
                        recipe.map((dish, index) => {
                            return (
                                <div className="card" key={index}>
                                    <p className="text">{dish.name}</p>
                                    <div className="percent">
                                        <svg>
                                            <circle cx="40" cy="40" r="40"></circle>
                                            <circle cx="40" cy="40" r="40"></circle>
                                        </svg>
                                        <div className="number">
                                            <p>{dish.margin.toFixed(0)}<span>%</span></p>
                                        </div>
                                    </div>
                                </div>
                            );
                        })
                    }
                </div>
            </div>

        </>
    )
}

export default HighMarginRecipes;
