import React from 'react';
import useFetch from "../CustomHooks/useFetch";
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

const FluctuatingRecipes = () => {

    const url = 'https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/fluctuation-group/?order=top';
    let { recipe } = useFetch(url);

    const styles = {
        iconStyle: {
            fontSize: 25
        }
    }

    return (
        <>
            <div className="container">
                <h3>Top Fluctuating Recipes</h3>
                <div className="circle-container">
                    {
                        recipe.map((dish, index) => {
                            return (
                                <div className="arrow-card" key={index}>
                                    <p className="text">{dish.name}</p>
                                    <hr />
                                    <div className="arrow">
                                        <p className={`${index === 0 ? "green" : "red"}`}>{dish.fluctuation.toFixed(0)}<span>%</span></p>
                                        <ArrowForwardIcon className={`${index === 0 ? "green" : "red rotate"}`} style={{ ...styles.iconStyle }} />
                                    </div>
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        </>
    )
}

export default FluctuatingRecipes;
