import React from 'react';
import FluctuatingRecipes from './FluctuatingRecipes';
import HighMarginRecipes from "./HighMarginRecipes";
import LowMarginRecipes from "./LowMarginRecipes";

const Cards = () => {

    return (
        <>
            <div className="main">
                <HighMarginRecipes />
                <LowMarginRecipes />
                <FluctuatingRecipes />
            </div>
        </>
    )
}

export default Cards;
